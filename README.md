![Screenshot preview of the theme "Empyrean" by glenthemes](https://64.media.tumblr.com/d1605e53918bfbbe3018a57fe5b6df73/9e5ce5a5570e2ccf-f1/s1280x1920/ebc90b53fe271bec2855d513decdd2a01fbdc2eb.png)

**Theme no.:** 37  
**Theme name:** Empyrean  
**Theme type:** Free / Tumblr use  
**Description:** *⟡~ em·py·re·an (adj.) — the highest heavens; the abode of God and the angels. featuring [Battle Queen Diana [Prestige Edition]](https://www.artstation.com/artwork/ea3Vl6) from League of Legends.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2020-12-29

**Post:** [glenthemes.tumblr.com/post/638859590535135232](https://glenthemes.tumblr.com/post/638859590535135232)  
**Preview:** [glenthpvs.tumblr.com/empyrean](https://glenthpvs.tumblr.com/empyrean)  
**Download:** [pastebin.com/kwQZ7Dep](https://pastebin.com/kwQZ7Dep)  
**Credits:** [glencredits.tumblr.com/empyrean](https://glencredits.tumblr.com/empyrean)
