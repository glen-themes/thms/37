// yo why do i have so much bullshit

$(document).ready(function(){
    // get height of sidebar frame
    // put desc text under the frame
    function on9() {
        var dis = $(".ff").height();
        var cue = $(".uma-tit").outerHeight() + $(".xwrap").outerHeight();
        $(".hc").height(dis+cue);
        
        $(".stuffbelow").css("margin-top",dis);
        
        var s = $(".desc").outerHeight();
        $(".cov").css("margin-top",-s);

        if(dis !== 0){
            $(".ff").css("visibility","visible")
        }
        
        var srz = $(".frame").attr("src");
        if(srz.indexOf('assets.tumblr.com/images/x.gif') >=0){
            $(".trans").show();
        }
    }
    
    on9();
    
    window.onresize = function() {
        on9();
    }
	
	var fart = Date.now();
    var jeff = setInterval(function(){
        if(Date.now() - fart > 10000){
            clearInterval(jeff);
        } else {
            on9();
        }
    },0);
	
	$(window).load(function(){
		clearInterval(jeff);
	});
    
    /*---------------------------*/
    
    $(".frame, .sf-border").on("dragstart", function(e){
        e.preventDefault();
    });
    
    /*---------------------------*/
    
    // show tags on click
    $(".clicktags").click(function(taggos) {
        $(this).parents(".hold").find(".teaseen").slideToggle(400);
        $(this).parents(".hold").find(".teaseen").toggleClass("theo");
    });
    
    // note to self: smoother animation of clicktags used to work
    // but it showed for all posts rather than the one focused lol
    
    /*
    $(document).on("click", ".clicktags", function(){
        
        $(".teaseen").slideDown(400);
        
        setTimeout(function(){
            $(".teaseen").addClass("theo");
        },200);
        
        setTimeout(function(){
            $(".clicktags").addClass("cheddar").removeClass("clicktags");
        },400);
    });
    
    $(document).on("click", ".cheddar", function(){
        $(".teaseen").removeClass("theo");
        
        setTimeout(function(){
            $(".teaseen").slideUp(400);
        },300);
        
        setTimeout(function(){
            $(".cheddar").addClass("clicktags").removeClass("cheddar");
        },400);
    });
    */
    
    /*---------------------------*/
    
    // BACKTOTOP BUTTON
    $(window).scroll(function(){
        if($(document).scrollTop() > $(document).height() * 0.1){
            $(".fraise").fadeIn(269);
        } else {
            $(".fraise").fadeOut(269);
        }
    });
    
    $(".fraise").click(function(){
        $("html").animate({
            scrollTop:0
        },900);
    });
    
    /*---------------------------*/
    
    // remove blog name on postnotes tooltips
    $("ol.notes").find("a").removeAttr("title");
    
    /*---------------------------*/
    
    // custom tooltips (general)
    $("a[title]:not(.square)").each(function(){
        var tit = $(this).attr("title");
        $(this).attr("data-title",tit);
        $(this).removeAttr("title");
    });
    
    // tooltips for post buttons
    $(".square[title]").each(function(){
        var vit = $(this).attr("title");
        $(this).find(".sqctn").attr("sq-title",vit);
        $(this).removeAttr("title");
    });
    
    /*---------------------------*/
    
    // in customization page, show photosets even if they're stretched
    if($(".tumblr_preview_marker___").length){
        $(".photoset-grid div").each(function(){
            var hhheight = $(this).find("img").width();
            $(this).height(hhheight)
        });
    }
    
    // remove tumblr preview marker by alyssa @thm97
    $(".tumblr_preview_marker___").remove();
    
    // once photosets are shown, remove the height
    if(!$(".tumblr_preview_marker___").length){
        $(".photoset-grid div").each(function(){
            $(this).css("height",null);
        });
    }
    
    // <figure> image in reblogs
    // if the last element is an image, double the padding
    // so that the border-bottom doesn't look awkward
    var captions_gap = parseInt(getComputedStyle(document.documentElement)
       .getPropertyValue("--Captions-Gap"));
    
    $(".post figure").each(function(){
        if(!$(this).hasClass("tmblr-full")){
            
            if($(".postscont").hasClass("oui")){
                if($(this).parents(".comment_container").next().length){
                    $(this).parents(".comment_container").css("padding-bottom",captions_gap);
                }
            }
        }
    });
    
    // if commenter is first-child, remove top whitespace
    $(".holyshit .commenter, .asker_").each(function(){
        if(!$(this).prev().length){
            $(this).css("margin-top",0)
        }
    });
    
    // remove blockquote whitespace
    $(".comment_container blockquote").each(function(){
        if(!$(this).prev().length){
            $(this).css("margin-top",0)
        }
        
        if(!$(this).next().length){
            $(this).css("margin-bottom",0)
        }
    });
    
    // remove headings whitespace
    $("h1, h2, h3, h4, h5, h6").each(function(){
        if(!$(this).parents(".comment_container").next().length){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        }
    });
    
    $("h1.title").each(function(){
        if(!$(this).prev().length){
            $(this).css("margin-top","0")
        }
    });
    
    // responsive videos & iframes
    flexibleFrames($(".video"));
    
    // turn npf_chat into a chat post
    $(".npf_chat").addClass("chatholder").removeClass("npf_chat");
    
    $(".chatholder").each(function(){
        if($(this).attr("data-npf")){
            $(this).find("b").addClass("chat_label");
            $(this).contents().last().wrap("<span class='chat_content'>")
        }
    });
    
    // add colon at the end of each chat_label
    $(".chat_label").each(function(){
        if(!$(this).text().endsWith(":")){
            $(this).append(":")
        }
    });
    
    // remove margins from empty <p>s
    $("p").each(function(){
        if($(this).is(':empty')){
            $(this).css("margin",0)
        }
    });
    
    $(".post p").each(function(){
        if(!$(this).prev(".commenter").length){
            $(this).css("margin-top",0)
        }
    });
    
    // if:reblog dividers, increase space between 1st commenter & the next
    var p_gap = getComputedStyle(document.documentElement)
               .getPropertyValue("--Paragraph-Margins"),
        numonly = parseFloat(p_gap),
        suffix = p_gap.split(/\d/).pop();
    
    $(".oui p").each(function(){
        if($(this).parents(".comment_container").next().length){
            $(this).css("margin-bottom",numonly / 2 + suffix)
        }
    });
    
    /*---------------------------*/
    
    // load svgs
    fetch("//glen-assets.github.io/theme-37-empyrean/freepik_repeat.html")
    .then(rt_svg => {
      return rt_svg.text()
    })
    .then(rt_svg => {
      $(".retweet").html(rt_svg);
    });
    
    
    fetch("//glen-assets.github.io/theme-37-empyrean/freepik_like.html")
    .then(like_svg => {
      return like_svg.text()
    })
    .then(like_svg => {
      $(".heart").html(like_svg)
    });
    
    
    fetch("//glen-assets.github.io/theme-37-empyrean/glenjamin.htm")
    .then(glen_svg => {
      return glen_svg.text()
    })
    .then(glen_svg => {
      $(".glenjamin").html(glen_svg)
    });
    
    /*---------------------------*/
    
    // show OP source if post does not have a caption
    // hide duplicate OP source if post *has* caption
    $(".post").each(function(){
        if($(this).find(".comment_container").length || $(this).find("body").length || $(this).find(".question_container").length){
            $(this).find(".src").hide();
        }
    });
    
    $(".src").each(function(){
        $(this).parents(".post").prepend($(this));
        $(this).css("margin-top",0);
        $(this).css("margin-bottom",captions_gap);
    });
    
    $(".caption").each(function(){
        if(!$(this).children().length){
            $(this).remove();
        }
    });
    
    // hide 'via' on quote posts
    $(".quote-source p").each(function(){
        var thestuff = "(via ";
        if($(this).text().startsWith(thestuff)){
            $(this).hide();
        }
    });
	
	// make iframe heights look more 'normal'
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();
            
            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;
            
            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;
            
            $(this).height(rat_h)
        }
    })

    // img max-width:100%
    $(".post img").each(function(){
        $(this).css("max-width","100%");
    });

    // line height
    var fontsize = parseInt(getComputedStyle(document.documentElement)
                  .getPropertyValue("--PostFontSize"));
    
    var LH = parseFloat(getComputedStyle(document.documentElement)
            .getPropertyValue("--PostLineHeight"));
    
    var shortanswer = (fontsize * LH) * 3;
    
    // only text-align:right if the answer text is shorter than 3 lines
    $(".answer_text").each(function(){
        if($(this).height() > shortanswer){
            $(this).css("text-align","left")
        }
    });

    // more npf
    $(".comment > p + .npf_inst").each(function(){
        if(!$(this).prev().prev().length){
            if($.trim($(this).prev().text()) == ""){
                $(this).addClass("photo-origin");
                $(this).insertBefore($(this).parents("[post-type='text']").find(".source-head").eq(0));
                $(this).css("margin-bottom","calc(var(--Captions-Gap) * 1.2)")
            }
        }
    })
    
});//end ready
